<?php
    require ("./inicializar.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Servidor UD04</title>
    <style>
        table , table td{
            border: 1px solid black;
        }
    </style>
</head>
<body>
    <table>
        <tr>
            <th>Usuario</th>
            <th>Email</th>
        </tr>
        <?php
            $conexion  = new Conexion();
            $conexion = $conexion->conectar();
            $sql = $conexion->prepare("SELECT nombre_usuario, email FROM usuarios;");
            $sql->execute();
            while ($row = $sql->fetch(PDO::FETCH_OBJ)){
                echo "<tr><td>";
                echo $row->nombre_usuario;
                echo "</td><td>";
                echo $row->email;
                echo "</td></tr>";
            }
            
        ?>
    </table>
</body>
</html>