<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>
        <?php
            $hoteles = array(
                        array("Nombre"=> "Abasshiri","Cat" => 3, "Hab"=> 168, "Poblacion" => "46013 Valencia", "Direccion" => "Av ausias March 59"),
                        array("Nombre"=> "Abba Acteon","Cat" => 4, "Hab"=> 189, "Poblacion" => "46023 Valencia", "Direccion" => "Escultor Vicente Veltran2"),
                        array("Nombre"=> "Acta Atarazanas","Cat" => 4, "Hab"=> 42, "Poblacion" => "46011 Valencia", "Direccion" => "Plaza tribunal 4"),
                        array("Nombre"=> "Acta del Carmen","Cat" => 3, "Hab"=> 25, "Poblacion" => "46003 Valencia", "Direccion" => "Blanquerias 11"),
                        array("Nombre"=> "AC Valencia","Cat" => 4, "Hab"=> 183, "Poblacion" => "46023 Valencia", "Direccion" => "Avenida Francia 67"),
                        array("Nombre"=> "Ad Hoc Monumental Valencia","Cat" => 3, "Hab"=> 28, "Poblacion" => "46003 Valencia", "Direccion" => "Boix 4"),
                        array("Nombre"=> "Alkazar","Cat" => 1, "Hab"=> 18, "Poblacion" => "46002 Valencia", "Direccion" => "Mosen femades 11")
            );
            
            function mlistado ($hotel){
                if (count($hotel)== 0){
                    echo "no hay mas hoteles en la base de datos";
                    return false;
                }
                for ($i = 0 ; $i < count($hotel) ; $i++){
                    echo "Nombre del hotel: " . $hotel[$i]["Nombre"] . "<br>";
                    echo "Categoria: " . $hotel[$i]["Cat"]  . "<br>";
                    echo "Numero habitaciones: " . $hotel[$i]["Hab"] . "<br>";
                    echo "Poblacion: " . $hotel[$i]["Poblacion"] . "<br>";
                    echo "Direccion: " . $hotel[$i]["Direccion"] . "<br>";
                }
            }

            function segunda ($hotel){
                $devolver = array();
                for ($i = 0 ; $i < count($hotel) ; $i++){
                    if ($hotel[$i]["Hab"]  > 100){
                        array_push($devolver, $hotel[$i]);
                    }
                }
                mlistado($devolver);
            }

            function tercera ($hotel){
                $devolver = array();
                for ($i = 0 ; $i < count($hotel) ; $i++){
                    if ($hotel[$i]["Hab"]  < 100  && $hotel[$i]["Cat"] == 3){
                        array_push($devolver, $hotel[$i]);
                    }
                }
                mlistado($devolver);
            }

            function cuarta ($hotel){
                $devolver = array();
                for ($i = 0 ; $i < count($hotel) ; $i++){
                    if ($hotel[$i]["Nombre"]  != "Acta del Carmen"){                        
                        array_push($devolver, $hotel[$i]);
                    }
                }
                mlistado($devolver);
                return $devolver;
            }

            function quinta ($hotel){
                $hotel = array();
                return $hotel;
            }

            function sexta ($hotel){
                array_push($hotel, array("Nombre"=> "Atoria palace","Cat" => 4, "Hab"=> 204, "Poblacion" => "4602 Valencia", "Direccion" => "Plaza rodrigo 5"));
                array_push($hotel, array("Nombre"=> "Balneario las arenas","Cat" => "lujo", "Hab"=> 253, "Poblacion" => "4611 Valencia", "Direccion" => "Eugenia viñes 22-24"));
                mlistado($hotel);
            }            
        ?>
    </h1>
</body>
</html>