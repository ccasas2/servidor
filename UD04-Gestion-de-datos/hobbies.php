<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario Aficiones</title>
</head>
<body>
    <h1>Formulario de Aficiones</h1>
    <?php 
        session_start();
        if(!empty($_SESSION['mensaje'])){ 
            echo "<p>" . $_SESSION['mensaje'] . "</p>"; 
            unset($_SESSION['mensaje']);
        } 
    ?>
    <form action="ejercicio3.php" method="post">
        <p><label for="nombre"><input type="text" id="nombre" name="nombre" required> Nombre</label>
        <label for="apellidos"><input type="text" id="apellidos" name="apellidos" required> Apellidos</label></p>
        <?php
            $checkboxes = ["hobby"=>"¿Que aficion prefieres?", 
                        "expense" => "¿Cuanto dinero crees que inviertes en ella?", 
                        "hour" => "¿Cuantas horas le dedicas a la semana?", 
                        "time" => "¿Cuanto tiempo te gustaría poder invertir?", 
                        "share" => "¿Compartes tus aficiones con álguien?"];
            $hobby = ["Deporte", "Cantar","Leer","Jugar a la play","Otros"];
            $expense = ["Nada", "menos de 20€ al mes", "entre 20€ y 100€ al mes", "más de 100€ al mes"];
            $hour = ["menos de 2 horas", "entre 2 y 5 horas","entre 5 y 10 horas","más de 10 horas"];
            $time = ["Quiero invertir menos tiempo", "El tiempo actual es suficiente","Me gustaria invertir entre 2 a 10 horas más","Todo el tiempo"];
            $share = ["Si", "No","Con mi grupo de amigos","Con familiares"];
        ?>       
        <?php foreach($checkboxes as $id => $title): ?>
            <h3><?php echo $title; ?></h3>
            <?php $contador = 0; ?>
            <?php foreach($$id as $opcion): ?>
                <p><input type="radio" id="<?php echo $id . $contador; ?>" 
                    name="<?php echo $id; ?>" value="<?php echo $contador; ?>" required>
                <label id="<?php echo "lb" . $id . $contador; ?>" for="<?php echo $id . $contador; ?>"> <?php echo $opcion; ?> </label>
                <?php $contador++; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
        <p><input type="submit" value="Enviar"></p>
    </form>    
</body>
</html>