<?php
    require ("./conexion.php");
    $con  = new Conexion();
    $conexion = $con->conectar();

    function iniciarBD ($cone){
        $bd = "ejerccicios_ud4";
        $sql = "CREATE DATABASE $bd";
        try{
            $cone->exec($sql);
            $cone->exec("use $bd");
            return true;
        }
        catch(PDOException $e){}
        try{
            $cone->exec("use $bd");
            return false;
        }
        catch(PDOException $e){
            echo $e;
            return false;
        }
    }

    function iniciarTablas($cone){
        $sqlUsuario = "CREATE TABLE `usuarios` (
            `nombre_usuario` VARCHAR(50) NOT NULL,
            `email` VARCHAR(45) NOT NULL,
            `contrasenya` VARCHAR(45) NOT NULL,
            PRIMARY KEY (`email`));";
        $sqlDirecciones = "CREATE TABLE `direcciones` (
            `id_direccion` INT NOT NULL,
            `calle` VARCHAR(50) NULL,
            `numero` INT NULL,
            `cp` INT NULL,
            `ciudad` VARCHAR(45) NULL,
            `provincia` VARCHAR(45) NULL,
            `pais` VARCHAR(45) NULL,
            `id_usuario` VARCHAR(50) NOT NULL,
            PRIMARY KEY (`id_direccion`),
            INDEX `id_usuario_idx` (`id_usuario` ASC) VISIBLE,
            CONSTRAINT `id_usuario`
              FOREIGN KEY (`id_usuario`)
              REFERENCES `usuarios` (`email`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION);";
        try{
            $cone->exec($sqlUsuario);
            $cone->exec($sqlDirecciones);
            return true;
        }
        catch(PDOException $e){
            return false;
        }
    }

    function rellenaTablas($cone){
        $usuarios = [["Carlos01", "carlos01@mail.es", "secret"], ["Carlos02", "carlos02@mail.es", "secret"],["Carlos03", "carlos03@mail.es", "secret"],["Carlos04", "carlos04@mail.es", "secret"]];
        $direcciones = [['1', 'calle1', '1', '46201', 'Paiporta', 'Valencia ', 'España', 'carlos01@mail.es'], ['2', 'calle2', '2', '46202', 'Paiporta', 'Valencia ', 'España', 'carlos02@mail.es'], ['3', 'calle3', '3', '46203', 'Paiporta', 'Valencia ', 'España', 'carlos03@mail.es'], ['4', 'calle1', '4', '46204', 'Paiporta', 'Valencia ', 'España', 'carlos04@mail.es']];
        for ($i=0; $i<4; $i++){
            $sql = "INSERT INTO `usuarios` (`nombre_usuario`, `email`, `contrasenya`) VALUES ('". $usuarios[$i][0]. "', '". $usuarios[$i][1]. "', '". $usuarios[$i][2]. "');";
            $cone->exec($sql);
            $sql = "INSERT INTO `direcciones` (`id_direccion`, `calle`, `numero`, `cp`, `ciudad`, `provincia`, `pais`, `id_usuario`) 
            VALUES ('". $direcciones[$i][0]. "', '". $direcciones[$i][1]. "', '". $direcciones[$i][2]. "', '". $direcciones[$i][3]. "', '". $direcciones[$i][4]. "', '". $direcciones[$i][5]. " ', '". $direcciones[$i][6]. "', '". $direcciones[$i][7]. "');";
            $cone->exec($sql);
        }
    }

    if (iniciarBD($conexion)){
        iniciarTablas($conexion);
        rellenaTablas($conexion);
    }
    
    unset($conexion);

?>