<?php

use Empleado as GlobalEmpleado;

    abstract class Trabajador{
        private string $nombre;
        private float $sueldo;
        public function __construct($nombre)
        {
            $this->nombre = $nombre;
        }
        public function getnombre(){
            return $this->nombre;
        }
        public function getsueldo (){
            return $this->sueldo;
        }
        protected function setsueldo($sueldo){
            $this->sueldo = $sueldo;
        }
        abstract public function calcularSueldo();
    }
    class Empleado extends Trabajador{
        private $hora = 9.5;
        public function __construct($nombre)
        {
            parent::__construct($nombre);
        }
        public function calcularSueldo(){
            parent::setsueldo(40 * 4 * $this->hora);
            return parent::getsueldo();
        }
    }

    class Gerente extends Trabajador{
        private $base = 2500;
        private $beneficio;
        public function __construct($nombre, $beneficio)
        {
            parent::__construct($nombre);
            $this->beneficio = $beneficio;
        }
        public function calcularSueldo(){
            parent::setsueldo($this->base + $this->beneficio * 0.1);
            return parent::getsueldo();
        }
    }
    $emp1 = new Empleado("juan");
    echo $emp1->calcularSueldo();
    $emp2 = new Gerente("pedro", 150000);
    echo $emp2->calcularSueldo();

?>