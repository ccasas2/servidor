<?php   
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        session_start();
        $directorio = './ficheros/';
        if (!file_exists($directorio)) {
            mkdir($directorio, 0755);
        } 
        $json = json_encode($_POST, JSON_PRETTY_PRINT);
        $total_archivos = count(glob($directorio . '*.json',GLOB_BRACE));
        $nombre_fichero = $directorio.$total_archivos.$_POST["nombre"]." ".$_POST["apellidos"].".json";
        $creado = file_put_contents($nombre_fichero, $json);
        if (!$creado){
            $_SESSION['mensaje'] = 'Error al guardar los datos';
        }
        else{
            $_SESSION['mensaje'] = 'Datos introducidos correctamente';
        }
    } 
    header("Location: hobbies.php");
?>