<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>
        <?php
            define("limite", 1000 );
            $aleatorio = rand(0, limite);
            $resultado = $aleatorio % 2 == 0 ? "es par" : "es impar";
            echo $resultado;
            echo limite;
        ?>
    </h1>
</body>
</html>