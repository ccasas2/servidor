<?php
    class Empleado{
        private string $nombre;
        private string $apellido;
        private float $sueldo;
        private $numero_empleado;
        public function __construct()
        {
            $this->numero_empleado = rand(0, 1500);
        }

        public function getnombre (){
            $devolver = [];
            array_push($devolver, $this->nombre);
            array_push($devolver, $this->apellido);
            return $devolver;
        }
        public function setnombre($nombre, $apellido){
            $this->nombre = $nombre;
            $this->apellido = $apellido;
        }
        public function getsueldo(){
            return $this->sueldo;
        }
        public function setsueldo($sueldo){
            $this->sueldo = $sueldo * 1.1; 
        }
        public function getnumero(){
            return $this->numero_empleado;
        }
        public function impuestos(){
            return ($this->sueldo >= 1200);
        }
    }
    $empleado1 = new Empleado;
    $empleado2 = new Empleado;
    $empleado1->setnombre("juan", "perez");
    $empleado1->setsueldo(1500);
    $empleado2->setnombre("pedro", "garcia");
    $empleado2->setsueldo(1501);
    echo $empleado1->getnombre()[0] . $empleado1->getnombre()[1] . " tiene un sueldo de: " . $empleado1->getsueldo() . "numero empleado: " . $empleado1->getnumero() .  "<br>";
    echo $empleado2->getnombre()[0] . $empleado2->getnombre()[1] . " tiene un sueldo de: " . $empleado2->getsueldo() . "numero empleado: " . $empleado2->getnumero() .  "<br>";
    
?>