<?php 
$nombre_fichero = "precios.txt";
$nombre_descuentos = "descuentos.txt";

$existe = file_exists($nombre_fichero) && is_readable($nombre_fichero);
    
$precios = fopen($nombre_fichero, "r");
$descuentos = fopen($nombre_descuentos, "w");

while (!feof($precios)){
    $linea = fgets($precios);
    $linea = str_replace(",", ".", $linea);
    $valor = floatval($linea);
    echo $linea . "-- <br>";
    echo $valor . "<br>";
    if ($valor < 50 ) {
        $valor = $valor * 0.95;
        fwrite($descuentos, $valor);
        fwrite($descuentos, "\n");
    }
    else{
        $valor = $valor * 0.90;
        fwrite($descuentos, $valor);
        fwrite($descuentos, "\n");
    }
}
fclose($precios);
fclose($descuentos);
?>