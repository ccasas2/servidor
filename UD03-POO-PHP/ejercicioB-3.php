<?php 
$user = $_POST["user"] ?? null;
$pass = $_POST["pass"] ?? null;
$idioma = $_POST["idioma"] ?? null;

$usuario = "ejercicio3";
$password = password_hash("1234", PASSWORD_DEFAULT, ['cost' => 15]);

function validar_usuario($user, $usuario, $pass, $password){
    return ($user == $usuario && password_verify($pass, $password));
}


if (!($user && $pass && $idioma)){
    echo "todos los campos son obligatorios";
}
else{
    setcookie("idioma", $idioma, time()+3600);  
    if (validar_usuario($user, $usuario, $pass, $password)){
        session_start();
        $_SESSION["login"] = true;
        $_SESSION["usuario"] = $user;
        $_SESSION["idioma"] = $idioma;
        header("Location:ejercicioB-3B.php");
    }
    else{
        echo ("Usuario no admitido");
    }
    
}
?>