<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>
        <?php
            $codigo = 1;
            $nombre = "Tom";
            $apellidos = "Smith";
            $puesto = "Vendedor";
            $sueldo = 75000;
            $edad = 26;
            $num_hijos = 0;
            $sucursal = "New York";

            $retencion = 0;

            $retencion += $puesto == "vendedor" && $sueldo > 70000 ? $sueldo * 0.1 : $sueldo * 0.2;
            $retencion += $edad > 50 || $num_hijos > 2 ? $sueldo * 0.05 : $sueldo * 0.1;
            $retencion += $sueldo > 50000 && $sueldo < 80000 ? $sueldo * 0.05 : $sueldo * 0.12;
            $retencion += $num_hijos == 1 || $num_hijos == 2 ? $sueldo * 0.1 : $sueldo * 0.05;
            $retencion += $sueldo > 80000 || $num_hijos == 0 ? $sueldo * 0.15 : $sueldo * 0.05;

            echo $retencion;

            
        ?>
    </h1>
</body>
</html>