<?php 
session_start();
$idioma = $_COOKIE["idioma"] ?? null;
$saludo = array ("ES" => "Bienvenido", "EN" => "Wellcome", "FR" => "Bienvenue");

function saludo($idioma, $saludo){
    echo ($saludo[$idioma]);
}

function boton_deslog (){
    echo '<form action="ejercicioB-3-destroy.php" method="post"> 
            <input type="submit" name="deslogear" value="Deslogear"/>
        </form>';
}

if (!((isset($_SESSION["login"]) && $_SESSION["login"]) && $idioma )) {
    echo "NO ESTAS LOGEADO COMO USUARIO CON ACCESO A ESTA SECCION";
}
else{
    saludo($idioma, $saludo);
    boton_deslog();
}
    
?>