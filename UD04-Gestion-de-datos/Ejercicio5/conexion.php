<?php
	class Conexion {
		var $tipo = "mysql";
		var $HOST_DB = "localhost";
		var $puerto = 3306;
		var $USER_DB = "root";
		var $PASS_DB = "secret";
		var $bd = "ejerccicios_ud4";
		function conectar (){
			try{
				$dsn = $this->tipo . ":host=" . $this->HOST_DB . ";dbname=" . $this->bd . ";port=" . $this->puerto;
				$conexion = new PDO ($dsn, $this->USER_DB, $this->PASS_DB);
				return $conexion;
			}
			catch (PDOException $e){
				echo $e->getMessage();
			}
		}
	}	
?>
