<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>
        <?php
            $edades = array(15,16,18,20,22);
            $alturas = array(1.75,1.80,1.2,1.9,2);
            $nun_alumnos = count($edades);
            $edad_media = 0;
            $altura_media = 0;
            $mayor_edad = 0;
            $altos = 0;
            for ($i = 0; $i< $nun_alumnos; $i++){
                $edad_media += $edades[$i];
                $altura_media += $alturas[$i];
                if ($edades[$i] >= 18){
                    $mayor_edad ++;
                }
                if ($alturas[$i] > 1.75){
                    $altos ++;
                }
            }
            echo "la altura media: " . $altura_media . "<br>";
            echo "la edad media: " . $edad_media  . "<br>";
            echo "mayores de 18: " . $mayor_edad  . "<br>";
            echo "mas de 1.75: ". $altos . "<br>";
        ?>
    </h1>
</body>
</html>