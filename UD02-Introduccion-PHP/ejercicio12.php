<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>
        <?php
            echo "Bucle for<br>";
            for ($i=0; $i<= 100; $i += 5){
                echo $i."<br>";
            }
            echo "Bucle while<br>";
            $i=0;
            while ($i <= 100){
                echo $i."<br>";
                $i += 5;
            }
            echo "Bucle do while<br>";
            $i=0;
            do{
                echo $i."<br>";
                $i +=5;
            }
            while ($i<=100);

        ?>
    </h1>
</body>
</html>