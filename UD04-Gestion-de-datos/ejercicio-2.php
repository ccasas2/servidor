<?php 
$nombre_fichero = "numeros.txt";
   
$numeros = fopen($nombre_fichero, "w");

$min = 1;
$max = 120;
$cantidad = 100;

for ($i = 0 ; $i < $cantidad ; $i++){
    $num = rand($min, $max);
    fwrite($numeros, $num);    
    fwrite($numeros, "\n");
}
fclose($numeros);

if (is_readable($nombre_fichero)){
    $numeros = fopen($nombre_fichero, "r");

    $numeros_pares = 0;
    $suma_numeros_impares = 0;
    $num_veces = array();

    While (!feof($numeros)){
        $linea = fgets($numeros);
        $valor = intval($linea);
        $num_veces[$valor] = ($num_veces[$valor] ?? 0) + 1;
        /*
        if (isset($num_veces[$valor])){
            $num_veces[$valor] = $num_veces[$valor] +1;
        }
        else{
            $num_veces[$valor] = 1;
        }
        */
        if ($valor %2){
            $numeros_pares ++;
        }
        else{
            $suma_numeros_impares += $valor;
        }
    }

    for ($i = $min ; $i <= $max ; $i++){
        if (isset($num_veces[$i])){
            echo "el valor $i aparece $num_veces[$i] veces";
        }
        else{
            echo "el valor $i aparece 0 veces";
        }
        echo "<br>";
    }


    echo "La cantidad de numeros pares es de: " . $numeros_pares . "<br>";
    echo "La suma de numeros impares es de: " . $suma_numeros_impares . "<br>";
}
else{
    echo "el fichero no se puede abrir";
}


?>