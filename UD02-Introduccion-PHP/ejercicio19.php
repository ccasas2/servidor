<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>
        <?php
            $alumnos = array(array('Nombre' => 'Juan Pérez', 'Edad' => 18, 'Altura' => 1.8), array('Nombre' => 'Aida López', 'Edad' => 21, 'Altura' => 1.75), array('Nombre' => 'Vanessa Costa', 'Edad' => 34, 'Altura' => 1.65), array('Nombre' => 'Daniel García', 'Edad' => 16, 'Altura' => 1.78)); //Inicializamos las variables 
            $facturas = array(
                        array("codigo"=> "1","cantidadvendida" => 10, "precio"=> 2),
                        array("codigo"=> "2","cantidadvendida" => 200, "precio"=> 3),
                        array("codigo"=> "3","cantidadvendida" => 30, "precio"=> 4),
                        array("codigo"=> "4","cantidadvendida" => 403, "precio"=> 5),
                        array("codigo"=> "5","cantidadvendida" => 50, "precio"=> 6)
            );
            $ftotal = 0;
            $cantuno = 0;
            $masdeseis = 0;
            for ($i= 0; $i < count($facturas);$i++){
                $ftotal += ($facturas[$i]["cantidadvendida"] * $facturas[$i]["precio"]);
                if ($facturas[$i]["codigo"] == "1"){
                    $cantuno ++;
                }
                if ($facturas[$i]["cantidadvendida"] * $facturas[$i]["precio"] >= 600){
                    $masdeseis ++;
                }
            }
            echo "Facturacion total: " . $ftotal . "<br>";
            echo "Facturas para el 1: " . $cantuno  . "<br>";
            echo "Facturas de mas de 600: " . $masdeseis . "<br>";
        ?>
    </h1>
</body>
</html>