<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>
        <?php
            $numero = array(1,2,3,4,-5,6,7,8,-9,0,1);
            $mediapositiva = 0;
            $num_positivos = 0;
            $medianegativa = 0;
            $num_negativos = 0;
            $num_ceros = 0;
            for ($i=0; $i < count($numero); $i++ ){
                if ($numero[$i] < 0){
                    $medianegativa += $numero[$i];
                    $num_negativos ++;
                }
                elseif ($numero[$i] > 0){
                    $mediapositiva += $numero[$i];
                    $num_positivos ++;
                }
                else{
                    $num_ceros ++;
                }
            }
            $medianegativa = $medianegativa / $num_negativos;
            $mediapositiva = $mediapositiva / $num_positivos;
            echo "la media es de positivos: " . ($mediapositiva) . "<br>";
            echo "la media es de negativos: " . ($medianegativa) . "<br>";
            echo "El numero de ceros : " . ($num_ceros) . "<br>";
        ?>
    </h1>
</body>
</html>