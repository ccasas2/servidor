<?php
    class Mamifero{
        private string $especie;
        private string $sonido;
        private string $familia;
        public function __construct($especie, $sonido)
        {
            $this->especie = $especie;
            $this->sonido = $sonido;
        }
        public function getespecie(){
            return $this->especie;
        }
        public function getsonido (){
            return $this->sonido;
        }
        public function getfamilia (){
            return $this->familia;
        }
        public function setfamlia ($familia){
            $this->familia = $familia;
        }
        public function sonido(){
            return "Sonido de " . $this->especie . "de la familia " . $this->familia . ": " . $this->sonido;
        }
    }
    class Perro extends Mamifero{
        public function __construct($especie, $sonido)
        {
            parent::__construct($especie, $sonido);
            parent::setfamlia("canido");
        }
    }
    class Gato extends Mamifero{
        public function __construct($especie, $sonido)
        {
            parent::__construct($especie, $sonido);
            parent::setfamlia("felino");
        }
    }




    $bicho1 = new Mamifero("asd", "cuac");
    $bicho1->setfamlia("casas");
    $bicho2 = new Perro("asd1", "asd2");
    echo $bicho2->sonido();
    $bicho3 = new Gato("asd1", "asd2");
    echo $bicho3->sonido();
?>